#!/usr/bin/perl -w

# Input: data/RuleSubSets_PerWord/*
# data/RuleSubSets_PerWord/$simtype.$language.rulword.$subset.$i

foreach $file (@ARGV){
	$info=$file;
	$info=~s/.+\/([^\/]+)$/$1/;
	($simtype, $lang, $gdext, $lineNoext, $part)=split(/\./,$info);

	$pairs=("evaluation/datasets/" . $lang . ".pairs.ids");

	$pairSets=("evaluation/datasets/" . $lang . ".stempairSets.PerWord." . $simtype . "." . $part . ".ids");
	$mapfile=("data/RuleMatrices/" . $lang . ".voc.ruleMatrix.PerWord." . $simtype . "." . $part . ".ids");

	$cooc=("evaluation/recomputed_sim_cooc/" . $lang . ".stempairSets.PerWord." . $simtype . "." . $part . ".ids.sim");
	$cordataCooc=("evaluation/correlation_data_PerWord/" 	. $lang . "." . $simtype . ".Coocsim." . $part . "_human");

	$conPair=("evaluation/recomputed_sim_context/PairsStemmed/"  . $lang . ".stempairSets.PerWord." . $simtype . "." . $part .".ids.sim");
	$cordataConPair=("evaluation/correlation_data_PerWord/" . $lang . "." . $simtype . ".ConPairsim." . $part . "_human");

	$conCTX=("evaluation/recomputed_sim_context/ContextStemmed/" . $lang .".pairs.PerWord." . $simtype . "." . $part . ".ids.sim");
	$cordataConCTX=("evaluation/correlation_data_PerWord/" 	. $lang . "." . $simtype . ".ConCTXsim." . $part . "_human");

	$conB=("evaluation/recomputed_sim_context/BothStemmed/" . $lang . ".stempairSets.PerWord." . $simtype . "." . $part .".ids.sim");
	$cordataConB=("evaluation/correlation_data_PerWord/" 	. $lang . "." . $simtype . ".ConBsim." . $part . "_human");

	system(`perl scripts/FilterRules_PerWord.pl $part $lang $simtype $lineNoext data/RuleMatrices/$lang\.voc.ruleMatrix1.ids`);
	system(`perl scripts/reCooSim.pl $pairSets $lang`);
	system(`perl scripts/reConSim_pairsStem.pl $pairSets $lang`);
	system(`perl scripts/reConSim_bothStem.pl $pairSets $mapfile $lang`);
	system(`perl scripts/reConSim_contextStem_PerWord.pl $pairs $mapfile $part $simtype $lang`);

	system(`paste evaluation/datasets/$lang.humanScores $cooc > $cordataCooc`);
	system(`perl scripts/PearsonCor.pl -i $cordataCooc > evaluation/correlations_PerWord/$lang.$simtype.Coocsim.CorSim.$part`);

	system(`paste evaluation/datasets/$lang.humanScores $conPair > $cordataConPair`);
	system(`perl scripts/PearsonCor.pl -i $cordataConPair > evaluation/correlations_PerWord/$lang.$simtype.ConPairsim.CorSim.$part`);

	system(`paste evaluation/datasets/$lang.humanScores $conCTX > $cordataConCTX`);
	system(`perl scripts/PearsonCor.pl -i $cordataConCTX > evaluation/correlations_PerWord/$lang.$simtype.ConCTXsim.CorSim.$part`);

	system(`paste evaluation/datasets/$lang.humanScores $conB > $cordataConB`);
	system(`perl scripts/PearsonCor.pl -i $cordataConB > evaluation/correlations_PerWord/$lang.$simtype.ConBsim.CorSim.$part`);

}#foeach_subset	

@langs=("en","el","de");

foreach $lang (@langs){

	# Copy extremes:
	# 1/ Copy no stemming (0)
	system(`cp evaluation/correlations/$lang.cooc.Coocsim.CorSim.0 evaluation/correlations_PerWord/$lang.net.Coocsim.CorSim.0`);
	system(`cp evaluation/correlations/$lang.cooc.Coocsim.CorSim.0 evaluation/correlations_PerWord/$lang.cooc.Coocsim.CorSim.0`);
	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_PerWord/$lang.cooc.ConPairsim.CorSim.0`);
	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_PerWord/$lang.net.ConPairsim.CorSim.0`);
 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_PerWord/$lang.cooc.ConCTXsim.CorSim.0`);
 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_PerWord/$lang.cooc.ConBsim.CorSim.0`);
 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_PerWord/$lang.net.ConCTXsim.CorSim.0`);
 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_PerWord/$lang.net.ConBsim.CorSim.0`);

 	# 2/ Copy full stemming (10)
 	system(`cp evaluation/correlations/$lang.cooc.Coocsim.CorSim.10 evaluation/correlations_PerWord/$lang.cooc.Coocsim.CorSim.10`);
 	system(`cp evaluation/correlations/$lang.net.Coocsim.CorSim.10 evaluation/correlations_PerWord/$lang.net.Coocsim.CorSim.10`);

 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.10 evaluation/correlations_PerWord/$lang.cooc.ConPairsim.CorSim.10`);
 	system(`cp evaluation/correlations/$lang.net.ConPairsim.CorSim.10 evaluation/correlations_PerWord/$lang.net.ConPairsim.CorSim.10`);

	system(`cp evaluation/correlations/$lang.cooc.ConCTXsim.CorSim.10 evaluation/correlations_PerWord/$lang.cooc.ConCTXsim.CorSim.10`);
	system(`cp evaluation/correlations/$lang.net.ConCTXsim.CorSim.10 evaluation/correlations_PerWord/$lang.net.ConCTXsim.CorSim.10`);

	system(`cp evaluation/correlations/$lang.cooc.ConBsim.CorSim.10 evaluation/correlations_PerWord/$lang.cooc.ConBsim.CorSim.10`);
	system(`cp evaluation/correlations/$lang.net.ConBsim.CorSim.10 evaluation/correlations_PerWord/$lang.net.ConBsim.CorSim.10`);

}#Copy extremes for each language

system(`perl scripts/makeCorTables_PerWord.pl evaluation/correlations_PerWord/*.CorSim.*`);




