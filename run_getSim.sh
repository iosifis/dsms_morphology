# Run getsimilarities for german:

perl new_scripts/get_similarities.pl de cooc new_data/RuleMatrices/de.voc.ruleMatrix1.ids > new_data/SimOfRulesMatrices/sim.cooc.de
perl new_scripts/get_similarities.pl de net new_data/RuleMatrices/de.voc.ruleMatrix1.ids > new_data/SimOfRulesMatrices/sim.net.de
perl new_scripts/get_similarities.pl en cooc new_data/RuleMatrices/en.voc.ruleMatrix1.ids > new_data/SimOfRulesMatrices/sim.cooc.en
perl new_scripts/get_similarities.pl en net new_data/RuleMatrices/en.voc.ruleMatrix1.ids > new_data/SimOfRulesMatrices/sim.net.en
perl new_scripts/get_similarities.pl el net new_data/RuleMatrices/el.voc.ruleMatrix1.ids > new_data/SimOfRulesMatrices/sim.net.el
