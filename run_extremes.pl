#!/usr/bin/perl -w

# Run no stemming & full stemming

@langs=("en","el","de");
@sims=("net", "cooc");

# No stemming:
# runs on pairs.ids

# foreach $lang (@langs){

# 	$pairs=("evaluation/datasets/" . $lang . ".pairs.ids");
# 	$cooc=("evaluation/recomputed_sim_cooc/" . $lang . ".pairs.ids.sim");
# 	$cordataCooc=("evaluation/correlation_data/" 	. $lang . ".cooc.Coocsim.0_human");
# 	$conPair=("evaluation/recomputed_sim_context/PairsStemmed/"  . $lang . ".pairs.ids.sim");
# 	$cordataConPair=("evaluation/correlation_data/" . $lang . ".cooc.ConPairsim.0_human");

# 	system(`perl scripts/reCooSim.pl $pairs $lang`); 
# 	system(`paste evaluation/datasets/$lang.humanScores $cooc > $cordataCooc`);
# 	system(`perl scripts/PearsonCor.pl -i $cordataCooc > evaluation/correlations/$lang.cooc.Coocsim.CorSim.0`);
# 	system(`cp evaluation/correlations/$lang.cooc.Coocsim.CorSim.0 evaluation/correlations/$lang.net.Coocsim.CorSim.0`);

# 	system(`perl scripts/reConSim_pairsStem.pl $pairs $lang`);
# 	system(`paste evaluation/datasets/$lang.humanScores $conPair > $cordataConPair`);
# 	system(`perl scripts/PearsonCor.pl -i $cordataConPair > evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0`);

# 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations/$lang.net.ConPairsim.CorSim.0`);
# 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations/$lang.cooc.ConCTXsim.CorSim.0`);
# 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations/$lang.cooc.ConBsim.CorSim.0`);
# 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations/$lang.net.ConCTXsim.CorSim.0`);
# 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations/$lang.net.ConBsim.CorSim.0`);

# }#foreach $lang	

$part=10;

# full stemming:

foreach $lang (@langs){

	system(`cut -f3,4 data/RuleMatrices/$lang.voc.ruleMatrix1.ids > data/RuleMatrices/$lang.voc.ruleMatrix.10.ids`);
		
	foreach $simtype(@sims){
	
		$pairSets=("evaluation/datasets/" . $lang . ".stempairSets1");
		$mapfile=("data/RuleMatrices/" . $lang . ".voc.ruleMatrix.10.ids");
		$pairs=("evaluation/datasets/" . $lang . ".pairs.ids");

		$cooc=("evaluation/recomputed_sim_cooc/" . $lang . ".stempairSets1.sim");
		$conPair=("evaluation/recomputed_sim_context/PairsStemmed/"  . $lang . ".stempairSets1.sim");
		$conCTX=("evaluation/recomputed_sim_context/ContextStemmed/"  . $lang . ".pairs." . $simtype . "." . $part . ".ids.sim");
		$conB=("evaluation/recomputed_sim_context/BothStemmed/"  . $lang . ".stempairSets1.sim");

		$cordataCooc=("evaluation/correlation_data/" 	. $lang . "." . $simtype . ".Coocsim." . $part . "_human");
		$cordataConPair=("evaluation/correlation_data/" . $lang . "." . $simtype . ".ConPairsim." . $part . "_human");
		$cordataConCTX=("evaluation/correlation_data/" 	. $lang . "." . $simtype . ".ConCTXsim." . $part . "_human");
		$cordataConB=("evaluation/correlation_data/" 	. $lang . "." . $simtype . ".ConBsim." . $part . "_human");

		system(`perl scripts/reCooSim.pl $pairSets $lang`);
		system(`perl scripts/reConSim_pairsStem.pl $pairSets $lang`);
		system(`perl scripts/reConSim_bothStem.pl $pairSets $mapfile $lang`);
		system(`perl scripts/reConSim_contextStem.pl $pairs $mapfile $part $simtype $lang`);

		system(`paste evaluation/datasets/$lang.humanScores $cooc > $cordataCooc`);
		system(`perl scripts/PearsonCor.pl -i $cordataCooc > evaluation/correlations/$lang.$simtype.Coocsim.CorSim.$part`);

		system(`paste evaluation/datasets/$lang.humanScores $conPair > $cordataConPair`);
		system(`perl scripts/PearsonCor.pl -i $cordataConPair > evaluation/correlations/$lang.$simtype.ConPairsim.CorSim.$part`);

		system(`paste evaluation/datasets/$lang.humanScores $conCTX > $cordataConCTX`);
		system(`perl scripts/PearsonCor.pl -i $cordataConCTX > evaluation/correlations/$lang.$simtype.ConCTXsim.CorSim.$part`);

		system(`paste evaluation/datasets/$lang.humanScores $conB > $cordataConB`);
		system(`perl scripts/PearsonCor.pl -i $cordataConB > evaluation/correlations/$lang.$simtype.ConBsim.CorSim.$part`);


	}#foreach $simtype
}#foreach $lang	
