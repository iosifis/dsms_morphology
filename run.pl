#!/usr/bin/perl -w

use Encode;
use utf8;

# Input: data/RuleSubSets/*

foreach $file (@ARGV){
	
	$info=$file;
	$info=~s/.+\/([^\/]+)$/$1/;
	($simtype, $lang, $gdext, $lineNoext, $part)=split(/\./,$info);

	$pairSets=("evaluation/datasets/" . $lang . ".stempairSets." . $simtype . "." . $part . ".ids");
	$mapfile=("data/RuleMatrices/" . $lang . ".voc.ruleMatrix." . $simtype . "." . $part . ".ids");
	$pairs=("evaluation/datasets/" . $lang . ".pairs.ids");
	
	$cooc=("evaluation/recomputed_sim_cooc/" . $lang . ".stempairSets." . $simtype . "." . $part . ".ids.sim");
	$conPair=("evaluation/recomputed_sim_context/PairsStemmed/"  . $lang . ".stempairSets." . $simtype . "." . $part .".ids.sim");
	$conCTX=("evaluation/recomputed_sim_context/ContextStemmed/" . $lang .".pairs." . $simtype . "." . $part . ".ids.sim");
	$conB=("evaluation/recomputed_sim_context/BothStemmed/" 	 . $lang . ".stempairSets." . $simtype . "." . $part .".ids.sim");

	$cordataCooc=("evaluation/correlation_data/" 	. $lang . "." . $simtype . ".Coocsim." . $part . "_human");
	$cordataConPair=("evaluation/correlation_data/" . $lang . "." . $simtype . ".ConPairsim." . $part . "_human");
	$cordataConCTX=("evaluation/correlation_data/" 	. $lang . "." . $simtype . ".ConCTXsim." . $part . "_human");
	$cordataConB=("evaluation/correlation_data/" 	. $lang . "." . $simtype . ".ConBsim." . $part . "_human");



	system(`perl scripts/FilterRules.pl $part $lang $simtype $lineNoext data/RuleMatrices/$lang\.voc.ruleMatrix1.ids`);
	system(`perl scripts/reCooSim.pl $pairSets $lang`);
	system(`perl scripts/reConSim_pairsStem.pl $pairSets $lang`);
	system(`perl scripts/reConSim_bothStem.pl $pairSets $mapfile $lang`);
	system(`perl scripts/reConSim_contextStem.pl $pairs $mapfile $part $simtype $lang`);

	system(`paste evaluation/datasets/$lang.humanScores $cooc > $cordataCooc`);
	system(`perl scripts/PearsonCor.pl -i $cordataCooc > evaluation/correlations/$lang.$simtype.Coocsim.CorSim.$part`);
	
	system(`paste evaluation/datasets/$lang.humanScores $conPair > $cordataConPair`);
	system(`perl scripts/PearsonCor.pl -i $cordataConPair > evaluation/correlations/$lang.$simtype.ConPairsim.CorSim.$part`);
	
	system(`paste evaluation/datasets/$lang.humanScores $conCTX > $cordataConCTX`);
	system(`perl scripts/PearsonCor.pl -i $cordataConCTX > evaluation/correlations/$lang.$simtype.ConCTXsim.CorSim.$part`);
	
	system(`paste evaluation/datasets/$lang.humanScores $conB > $cordataConB`);
	system(`perl scripts/PearsonCor.pl -i $cordataConB > evaluation/correlations/$lang.$simtype.ConBsim.CorSim.$part`);
	
}#foreach rule part
