#!/usr/bin/perl -w

# Input: data/RuleSubSets_RulFreq/*
# data/RuleSubSets_RulFreq/simtype.$language.rulgdf.$subset(lines).$i(part)

foreach $file (@ARGV){
	$info=$file;
	$info=~s/.+\/([^\/]+)$/$1/;
	($simtype, $lang, $gdext, $lineNoext, $part)=split(/\./,$info);

	$pairs=("evaluation/datasets/" . $lang . ".pairs.ids");

	$pairSets=("evaluation/datasets/" . $lang . ".stempairSets.RulFreq." . $simtype . "." . $part . ".ids");
	$mapfile=("data/RuleMatrices/" . $lang . ".voc.ruleMatrix.RulFreq." . $simtype . "." . $part . ".ids");

	$cooc=("evaluation/recomputed_sim_cooc/" . $lang . ".stempairSets.RulFreq." . $simtype . "." . $part . ".ids.sim");
	$cordataCooc=("evaluation/correlation_data_RulFreq/" 	. $lang . "." . $simtype . ".Coocsim." . $part . "_human");

	$conPair=("evaluation/recomputed_sim_context/PairsStemmed/"  . $lang . ".stempairSets.RulFreq." . $simtype . "." . $part .".ids.sim");
	$cordataConPair=("evaluation/correlation_data_RulFreq/" . $lang . "." . $simtype . ".ConPairsim." . $part . "_human");

	$conCTX=("evaluation/recomputed_sim_context/ContextStemmed/" . $lang .".pairs.RulFreq." . $simtype . "." . $part . ".ids.sim");
	$cordataConCTX=("evaluation/correlation_data_RulFreq/" 	. $lang . "." . $simtype . ".ConCTXsim." . $part . "_human");

	$conB=("evaluation/recomputed_sim_context/BothStemmed/" . $lang . ".stempairSets.RulFreq." . $simtype . "." . $part .".ids.sim");
	$cordataConB=("evaluation/correlation_data_RulFreq/" 	. $lang . "." . $simtype . ".ConBsim." . $part . "_human");

	system(`perl scripts/FilterRules_RulFreq.pl $part $lang $simtype $lineNoext data/RuleMatrices/$lang\.voc.ruleMatrix1.ids`);
	system(`perl scripts/reCooSim.pl $pairSets $lang`);
	system(`perl scripts/reConSim_pairsStem.pl $pairSets $lang`);
	system(`perl scripts/reConSim_bothStem.pl $pairSets $mapfile $lang`);
	system(`perl scripts/reConSim_contextStem_RulFreq.pl $pairs $mapfile $part $simtype $lang`);

	system(`paste evaluation/datasets/$lang.humanScores $cooc > $cordataCooc`);
	system(`perl scripts/PearsonCor.pl -i $cordataCooc > evaluation/correlations_RulFreq/$lang.$simtype.Coocsim.CorSim.$part`);

	system(`paste evaluation/datasets/$lang.humanScores $conPair > $cordataConPair`);
	system(`perl scripts/PearsonCor.pl -i $cordataConPair > evaluation/correlations_RulFreq/$lang.$simtype.ConPairsim.CorSim.$part`);

	system(`paste evaluation/datasets/$lang.humanScores $conCTX > $cordataConCTX`);
	system(`perl scripts/PearsonCor.pl -i $cordataConCTX > evaluation/correlations_RulFreq/$lang.$simtype.ConCTXsim.CorSim.$part`);

	system(`paste evaluation/datasets/$lang.humanScores $conB > $cordataConB`);
	system(`perl scripts/PearsonCor.pl -i $cordataConB > evaluation/correlations_RulFreq/$lang.$simtype.ConBsim.CorSim.$part`);

}#foeach_subset	

@langs=("en","el","de");

foreach $lang (@langs){

	# Copy extremes:
	# 1/ Copy no stemming (0)
	system(`cp evaluation/correlations/$lang.cooc.Coocsim.CorSim.0 evaluation/correlations_RulFreq/$lang.net.Coocsim.CorSim.0`);
	system(`cp evaluation/correlations/$lang.cooc.Coocsim.CorSim.0 evaluation/correlations_RulFreq/$lang.cooc.Coocsim.CorSim.0`);
	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_RulFreq/$lang.cooc.ConPairsim.CorSim.0`);
	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_RulFreq/$lang.net.ConPairsim.CorSim.0`);
 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_RulFreq/$lang.cooc.ConCTXsim.CorSim.0`);
 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_RulFreq/$lang.cooc.ConBsim.CorSim.0`);
 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_RulFreq/$lang.net.ConCTXsim.CorSim.0`);
 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.0 evaluation/correlations_RulFreq/$lang.net.ConBsim.CorSim.0`);

 	# 2/ Copy full stemming (10)
 	system(`cp evaluation/correlations/$lang.cooc.Coocsim.CorSim.10 evaluation/correlations_RulFreq/$lang.cooc.Coocsim.CorSim.10`);
 	system(`cp evaluation/correlations/$lang.net.Coocsim.CorSim.10 evaluation/correlations_RulFreq/$lang.net.Coocsim.CorSim.10`);

 	system(`cp evaluation/correlations/$lang.cooc.ConPairsim.CorSim.10 evaluation/correlations_RulFreq/$lang.cooc.ConPairsim.CorSim.10`);
 	system(`cp evaluation/correlations/$lang.net.ConPairsim.CorSim.10 evaluation/correlations_RulFreq/$lang.net.ConPairsim.CorSim.10`);

	system(`cp evaluation/correlations/$lang.cooc.ConCTXsim.CorSim.10 evaluation/correlations_RulFreq/$lang.cooc.ConCTXsim.CorSim.10`);
	system(`cp evaluation/correlations/$lang.net.ConCTXsim.CorSim.10 evaluation/correlations_RulFreq/$lang.net.ConCTXsim.CorSim.10`);

	system(`cp evaluation/correlations/$lang.cooc.ConBsim.CorSim.10 evaluation/correlations_RulFreq/$lang.cooc.ConBsim.CorSim.10`);
	system(`cp evaluation/correlations/$lang.net.ConBsim.CorSim.10 evaluation/correlations_RulFreq/$lang.net.ConBsim.CorSim.10`);

}#Copy extremes for each language

system(`perl scripts/makeCorTables__RulFreq.pl evaluation/correlations__RulFreq/*.CorSim.*`);




