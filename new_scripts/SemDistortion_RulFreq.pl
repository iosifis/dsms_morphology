#!/usr/bin/perl

# Version 2: new semantic distortion estimation based on delta normalisation
# Version 3 / June 2014: Corrected hash declarations

use Encode;
use utf8;
use Statistics::Descriptive;

# Expected Input file format:
#------------------------------
# RuleID<tab>StemID<tab>Wi_ID<tab>WordSet_IDs<tab>SimilaritiesSet<newline>
# where: WordSet_IDs and SimilaritiesSet are comma separated lists

# Input dir: data/SimOfRulesMatrices/sim.$simtype[net|cooc].$language
# Output dir: data/GlobalDistortion_RulFreq/$simtype.$language.rulgdf

$outdir=("new_data/GlobalDistortion_RulFreq");
mkdir ($outdir) unless (-d $outdir);



@files=@ARGV;       # input filenames

foreach $file (@files){

	$dir=$simtype=$language="";

	$info=$file;
	($dir, $simtype, $language)=split(/\./, $info);

	#-------------------------------------------
	# Get word frequency information as array:
	#-------------------------------------------

	$freqResource=("data/FrequenciesOfVocabWords/vocab.$language.freq");

	@frequ=();

	open (R,"$freqResource") && printf(STDERR "$0: Reading frequencies file $freqResource\n") || die ("$0: cannot read frequencies file: .. $freqResource ..\n");

		@frequ=<R>;

	close(R);

	#-----------------------------------------------
	# Now process similarity file:
	#-----------------------------------------------

	open (I,"$file") && printf(STDERR "$0: Reading input file $file\n") || die ("$0: cannot read input file: .. $file ..\n");

		%ruleSemDistortionNom=();
		%ruleSemDistortionDenom=();
		%ruleApplFrequ=();
		%ruleNorm=();

		while ($l = <I>){
        	 $l=~s/\s*$//;
         	 $l= decode('UTF-8', $l);

         	 @all=@data=();
         	 $sims=$wi_ID=$rule_ID="";
         	 $stat=$mean=$distance_sim=$frequ_wi=$delta=$distortionNom=$distortionDenom=0;

         	 #-----------------------------------
         	 # Calculate s(Wi, Lk(Wi)) 
         	 #		as average similarity between Wi and set of words with common stem to word Wi:

         	 @all=split("\t", $l);

         	 if(scalar(@all) == 5){ 		# if there is a sim value, element No 4 might be empty...

         	 	 $sims=$all[4];
	         	 @data=split(/\,/, $sims);
	         	 $wi_ID=$all[2];		# element at position 2 should be the Wi ID number
	         	 $rule_ID=$all[0];
	         	 $ruleApplFrequ{$rule_ID}++;

	         	 $stat = Statistics::Descriptive::Full->new();	# Create a new statistics object
	         	 $stat->add_data(@data);						# Add data to the statistics variable

	         	 # Values in net similary may be above 1, 
	         	 # for this reason we can normalise as 0-1 
	         	 # by dividing average by number of neighbours in net only:
	         	 
	         	 if($simtype eq "net"){
	         	 	$mean=$stat->mean();						# Get average semantic similarity
	         	 	$mean=($mean/150);							# normalise net $mean
	         	 }
	         	 else{
	         	 	$mean=$stat->mean();						# Get average semantic similarity
	         	 }

	         	 #-----------------------------------
	         	 # Calculate distance Wi as: 1- s(Wi, Lk(Wi))

	         	 $distance_sim=( 1 - $mean );

	         	 #-----------------------------------
	         	 # Get frequency of w_i:
	         	 			
	         	 $frequ_wi=( $frequ[$wi_ID-1] + 1 );	# make sure frequency is never 0
	         	 										# index of frequ array starts from 0, so $wi_ID-1

	         	 #-----V2: Get δ -------------------
	         	 if($rule_ID eq "NULL"){
	         	 	$delta=1;
	         	 }
	         	 else{
	         	 	$delta=0;
	         	 }
	         	 
	         	 #-----------------------------------
	         	 # Save distortion for each rule:

	         	 $distortionNom=($frequ_wi * $distance_sim);
	         	 $distortionDenom=($frequ_wi * (1 - $delta));
	         	 push(@{$ruleSemDistortionNom{$rule_ID}}, $distortionNom);
	         	 push(@{$ruleSemDistortionDenom{$rule_ID}}, $distortionDenom);

	         	 push(@{$ruleNorm{$rule_ID}}, $frequ[$wi_ID-1]+1);


	         }#if_there is a sim value

         }#while_input 	 
	close (I);

	@frequ=(); # empty frequ that is no longer needed 

	#----------------------------------------------------
	# Now calculate global distortion per stemming rule:

	%GlobalDistortions=();

	foreach $rule_ID (sort (keys(%ruleSemDistortionNom))) { # for each "rule" ie suffix

		$globalDistortionNominator=0;
		$globalDistortionDenominator=0;							
		$globalNorm=$stat2=$nominator=$denominator=0;
		
		$nominator = Statistics::Descriptive::Full->new(); 
		$nominator->add_data(@{$ruleSemDistortionNom{$rule_ID}});

		$denominator = Statistics::Descriptive::Full->new(); 
		$denominator->add_data(@{$ruleSemDistortionDenom{$rule_ID}});

		$globalDistortionNominator=$nominator->sum();
		$globalDistortionDenominator=$denominator->sum();

		$stat2 = Statistics::Descriptive::Full->new(); 
		$stat2->add_data(@{$ruleNorm{$rule_ID}});
		$globalNorm = $stat2->sum();		# total frequency of words where this rule applies

		if($globalDistortionDenominator){
			$GlobalDistortions{$rule_ID}{'distortion'}=$globalDistortionNominator/$globalDistortionDenominator;
			$GlobalDistortions{$rule_ID}{'rulFreq'}=$ruleApplFrequ{$rule_ID};
			$GlobalDistortions{$rule_ID}{'wordFreq'}=$globalNorm;

		}
		else{
			$GlobalDistortions{$rule_ID}{'distortion'}=("NA");
			$GlobalDistortions{$rule_ID}{'rulFreq'}=$ruleApplFrequ{$rule_ID};
			$GlobalDistortions{$rule_ID}{'wordFreq'}=$globalNorm;
		}

	}#foreach_stemming_rule

	%ruleSemDistortionNom=(); # empty hash that is no longer needed
	%ruleSemDistortionDenom=(); # empty hash that is no longer needed

	#--------------------------------------------------
	# Sort by global distortion value & by rule freq and print output:	
	#--------------------------------------------------
	$out=("$outdir/$simtype.$language.rulgdf");
    open (O,">$out") && printf(STDERR "$0: Writing output to $out\n") || die ("$0: cannot write to output file: .. $out ..\n");

    	# Sort GlobalDistortions hash by increasing global distortion value:

	    foreach $rule_ID (sort { 
	    				$GlobalDistortions{$a}{'distortion'} <=> $GlobalDistortions{$b}{'distortion'} 
	    			&&  $GlobalDistortions{$b}{'rulFreq'} <=> $GlobalDistortions{$a}{'rulFreq'} 
	    			} keys(%GlobalDistortions) ) {
	    	
		    	$globalDistortion=$GlobalDistortions{$rule_ID}{'distortion'};
		    	$ruleFrequ=$GlobalDistortions{$rule_ID}{'rulFreq'};
		    	$globalNorm=$GlobalDistortions{$rule_ID}{'wordFreq'};	

		    	$rule_ID= encode('UTF-8', $rule_ID);
		    	print O ("$rule_ID\t$globalDistortion\t$ruleFrequ\t$globalNorm\n");		   
	    	
	    }#foreach global distortion value

	close(O);

# empty all hashes (just in case)
	%ruleSemDistortionNom=(); 
	%ruleSemDistortionDenom=(); 
	%GlobalDistortions=();
	%ruleNorm=();
	%ruleApplFrequ=();
	@frequ=();

}#foreach_input_file



