#!/usr/bin/perl -w

use Encode;
use utf8;

# Input: eg evaluation/correlations/$lang.$simtype.ConCTXsim.CorSim.$part
# usage: perl scripts/makeCorTables.pl $outdir evaluation/correlations/*.CorSim.*

$outdir=shift(@ARGV);
chomp($outdir);
mkdir ($outdir) unless (-d $outdir);

%filehash=();

foreach $file (@ARGV){

	$filename=$file;
	$filename=~s/.+\/([^\/]+)$/$1/; # remove path
	($lang, $simtype, $retype, $ext, $part)=split(/\./,$filename);
	

	open (F,$file) && printf (STDERR "$0: Reading $file\n")|| die ("$0: cannot open $file\n");

		$l=<F>;
		$l=~s/^\s*//;
		$l=~s/\s*$//;

	close(F);

	$filehash{$lang}{$simtype}{$retype}{$part}=$l;

}#foreach

foreach $lang (keys %filehash){
	foreach $simtype (keys %{$filehash{$lang}}){
		foreach $retype (keys %{$filehash{$lang}{$simtype}}){

			$out=("$outdir/$lang\.$simtype\.$retype");

			open (O, ">$out") || die ("$0: cannot open $file\n");
				foreach $part (sort {$a <=> $b} keys %{$filehash{$lang}{$simtype}{$retype}}){

					$cor=$filehash{$lang}{$simtype}{$retype}{$part};
					print O ("$part\t\t$cor\n");
						
				}#foreach part in order

			close(O);	
		}#foreach $retype

	}#foreach $simtype

}#foreach_lang