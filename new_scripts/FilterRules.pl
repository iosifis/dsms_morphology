#!/usr/bin/perl -w

use Encode;
use utf8;

# Objective: For each rule selected in the subset, get word IDs
# Input: data/RuleMatrices/<language>.voc.ruleMatrix1.ids 
#        resource: data/RuleSubSets/$simtype.$language.rulgd\.<PART> ($ext)
# Outputs: 
#	1/ data/RuleMatrices/$language.voc.ruleMatrix.$simtype.<PART>.ids
#   2/ evaluation/datasets/$language.stempairSets.$simtype.<PART>($ext).ids


$ext=$ARGV[0];
$language=$ARGV[1];
$simtype=$ARGV[2];
$lineNoext=$ARGV[3];
$file=$ARGV[4];
chomp($file);

print STDERR ("$0: LANGUAGE: $language SIMTYPE: $simtype\n");

#-------------------------------------------
# Get stemming rules subset 
#-------------------------------------------

$rulResource=("new_data/RuleSubSets/" . $simtype . "." . $language . ".rulgd." . $lineNoext . "." . $ext);
%rules=();

open (R,"$rulResource") && printf(STDERR "$0: Reading rule subsets file $rulResource\n") || die ("$0: cannot read rule subsets file: .. $rulResource ..\n");

	while($r=<R>){

		$r=~s/\s*$//;
 		$r= decode('UTF-8', $r);

 		($ruleID, $distortion, $rulefreq, $wordfreq)=split(/\t/, $r);
 		$rules{$ruleID}=1;

	}#while_rulefile input
close(R);

%stem2wordID=();

# Filter input rule file:

open (I,"$file") && printf(STDERR "$0: Reading input file $file\n") || die ("$0: cannot read input file: .. $file ..\n");
	    
	while ($l = <I>){
		$l=~s/\s*$//;
 		$l= decode('UTF-8', $l);

 		($ruleID, $stem, $wordID, $wordSetIDs)=split(/\t/, $l);

 		if (defined $rules{$ruleID}){ 			
 			push (@{$stem2wordID{$stem}}, $wordID);
 		}#if_defined
 		
	}#while_inputline	
close(I);

%rules=();		# empty this hash
%wordID2Set=(); # hash for mapping word IDs to sets

# $out=$file;
# $out=~s/1\.ids$/\.$simtype\.$ext\.ids/;
$out=("new_data/RuleMatrices/$language.ruleMatrix.$simtype.$ext.ids");

open (O,">$out") && printf(STDERR "$0: Writing output to $out\n") || die ("$0: cannot write to output file: .. $out\n");
	
	foreach $stem (keys %stem2wordID){

		@wordIDsSet=@{$stem2wordID{$stem}};

		if(@wordIDsSet > 1){

			$allwords=join("\,", @wordIDsSet);

			foreach $wordID (@wordIDsSet){

				print O ("$wordID\t$allwords\n");
				$wordID2Set{$wordID}=$allwords;		# hash for mapping word IDs to sets in the pairs eval file 
						
			}#foreach wordID
			
		}#if > 1
			
	}#foreach_stem		
			
 close(O);

%stem2wordID=(); # empty this hash


$pairsResource=("evaluation/datasets/$language.pairs.ids"); 
$outPairs=("new_evaluation/datasets/$language.stempairSets.$simtype.$ext.ids");
open (R2, "$pairsResource")&& printf(STDERR "$0: Reading evaluation pairs IDs file $pairsResource\n") || die ("$0: cannot read evaluation pairs IDs file .. $pairsResource ..\n");
open (O2,">$outPairs") && printf(STDERR "$0: Writing output pair Sets to $outPairs\n") || die ("$0: cannot write to output pair Sets file: .. $outPairs\n");

while($p=<R2>){
	  $p=~s/\s*$//;

	  ($waID, $wbID)=split(/\t/, $p);

	  if(defined $wordID2Set{$waID}){
	  		$waIDset=$wordID2Set{$waID};
	  }
	  else{
	  		$waIDset=$waID;
	  }

	  if(defined $wordID2Set{$wbID}){
	  		$wbIDset=$wordID2Set{$wbID};
	  }
	  else{
	  		$wbIDset=$wbID;
	  }

	  print O2 ("$waIDset\t$wbIDset\n");

}#while_pair ids file


close(O2);
close(R2);





