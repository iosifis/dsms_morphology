#!/usr/bin/perl -w


## Input args
##-----------
$infile = $lang =  "";
$infile =  $ARGV[0]; chomp($infile); ## Input file
## Format of input file
## ID1,ID2,...IDn ID1,D2,..,IDm
## ID1,ID2,...IDn: correspond to the 1st word of the pair
## ID1,ID2,...IDm: correspond to the 2nd word of the pair
$lang = $ARGV[1]; chomp($lang); ## Similarity type: [el|en|de]


## Output
##-------
## The output file is written in the following directory:
## "../evaluation/recomputed_sim_cooc/"
## The name of the outputfile is the same as the input file + the extension ".sim"

$num_in_args = $#ARGV + 1;
if ($num_in_args != 2)
 {
   print ("$0: Wrong number of input arguments.\n");
   print ("2 input arguments are required.\n");
   die ("HOW TO RUN: perl scriptname.pl inputfile [el|en|de]  \n");
 }


## Input paths
##---------------------------------
$data_root_dir = $lang_data_dir = $split_data_dir = "";
#$data_root_dir = "../../../data/data/"; ## Root dir of data
$data_root_dir = "../../data/data/"; ## Root dir of data
$lang_data_dir = $data_root_dir.$lang."/"; 
$split_data_dir = $lang_data_dir."split_cooc_freq/"; ## Directory of split cooc freq

## File containing the freq of individual words
##----------------------------------------------
#$single_word_freq_file = "../data/FrequenciesOfVocabWords/"."vocab.".$lang.".freq";
$single_word_freq_file = "data/FrequenciesOfVocabWords/"."vocab.".$lang.".freq";

## Load freq of individual words
##------------------------------
%wID2f = ();
$wID = 0;
print STDERR ("$0 Loading frequencies of single words: start\n");
open (F,$single_word_freq_file) || die "$0: cannot open $single_word_freq_file \n";
while ($r  =<F>)
 {
  $wID ++;
  chomp($r);
  $fr = $r + 1;
  $wID2f{$wID} = $fr;
 }
close (F);
print STDERR ("$0: Loading frequencies of single words: end\n");



## Create output dirs and file
##-----------------------------
#$out_root_dir = "../evaluation/";
$out_root_dir = "new_evaluation/";
$out_d1 = $out_root_dir."recomputed_sim_cooc/";
#$out_d2 = $out_d1.$lang."/";

mkdir ($out_d1) unless (-d $out_d1);
#mkdir ($out_d2) unless (-d $out_d2);
#mkdir ($out_d3) unless (-d $out_d3);

$out=$infile;
$out=~s/^.+\/([^\/]+)$/$1/;  #remove dir path from filename

#$out_sim_file = $out_d3.$infile.".sim";
#$out_sim_file = $out_d3.$out.".sim";
$out_sim_file = $out_d1.$out.".sim";
open (O,">$out_sim_file") && printf (STDERR "$0: Writing output Cooc sim file $out_sim_file\n") || die ("$0: cannot write $out_sim_file\n");


## Compute:
## (1) frequencies for individual words
## (2) co-occurrence frequencies
## (3) corresponding similarity using (1) and (2)
## The similarity is computed as the Dice coefficient
##----------------------------------------------------
$r = "";
$c = 0;
open (I,$infile) || die ("$0: Cannot open similarity matrix $infile\n"); ## Process  input file line-by-line
while ($r = <I>)
 {
   $c ++;
   chomp($r);
   #print ($c,". Processing input: ",$r,"\n");

   ## Get the 2 basic fields of input
   ##--------------------------------
   $set1str = $set2str = "";
   @set1 = @set2 = ();
   ($set1str,$set2str) = split(/\s+/,$r);
   @set1 = split(/,/,$set1str);
   @set2 = split(/,/,$set2str);

   ## Iterate over the elements of the two sets
   ##-------------------------------------------
   $w1freq = $w2freq = 0;
   $w1w2coocfreq = 0;
   $re_computed_sim = 0;
   $el1 = $el2 = "";
   foreach $el1 (@set1)
    {
      #print ("  Set 1 - Current ID: ",$el1,"\n");
      if (defined($wID2f{$el1})) { $w1freq += $wID2f{$el1}; } ## Update freq for 1st word
      ## Load respective file of co-occurrence freq
      ##-------------------------------------------
      $cooc_freq_str = "";
      $current_cooc_freq_file = "";
      $current_cooc_freq_file = $split_data_dir.$el1;
      open (CF,$current_cooc_freq_file) || die "$0: cannot open $current_cooc_freq_file\n";
      $cooc_freq_str = <CF>;
      close (CF);
      @entries = ();
      @entries = split(/\s+/,$cooc_freq_str);
      %wID2coocf = ();
      foreach (@entries)
       {
         ($ww,$ccf) = split(/,/,$_);
         $wID2coocf{$ww} = $ccf;
       }

      foreach $el2 (@set2)
       {
         #print ("    Set 2 - Current ID: ",$el2,"\n");
         if (defined($wID2coocf{$el2})) { $w1w2coocfreq += $wID2coocf{$el2}; } ## Update co-occurrence frequency
       } ## For each element of set 2

    } ## For each element of set 1

  foreach $el2 (@set2)
   {
     if (defined($wID2f{$el2})) { $w2freq += $wID2f{$el2}; } ## Update freq for 2nd  word
   }

  #print ("Freq for 1st word: ",$w1freq,"\n");
  #print ("Freq for 2nd word: ",$w2freq,"\n");
  #print ("Co-occurrence freq: ",$w1w2coocfreq,"\n");

  if (($w1freq>0)&&($w2freq>0)&&($w1w2coocfreq>0))
   {
     $re_computed_sim = $w1w2coocfreq/($w1freq+$w2freq); ## Similarity: Dice coefficient
   }
  else
   {
     $re_computed_sim = 0;
   }

 #print ("Re-computed similarity: ",$re_computed_sim,"\n");
 print O ($re_computed_sim,"\n"); ## Write similarity to output file

 } ## For each line of input file
 
close (I);
close (O);
