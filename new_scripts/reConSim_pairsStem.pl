#!/usr/bin/perl -w


## Input args
##-----------
$infile = $lang =  "";
$infile =  $ARGV[0]; chomp($infile); ## Input file
## Format of input file
## ID1,ID2,...IDn ID1,D2,..,IDm
## ID1,ID2,...IDn: correspond to the 1st word of the pair
## ID1,ID2,...IDm: correspond to the 2nd word of the pair
$lang = $ARGV[1]; chomp($lang); ## Similarity type: [el|en|de]


## Output
##-------
## The output file is written in the following directory:
## "../evaluation/recomputed_similarities_context/"
## The name of the outputfile is the same as the input file + the extension ".sim"

$num_in_args = $#ARGV + 1;
if ($num_in_args != 2)
 {
   print ("$0: Wrong number of input arguments.\n");
   print ("2 input arguments are required.\n");
   die ("HOW TO RUN: perl scriptname.pl inputfile [el|en|de]  \n");
 }


## Input paths
##---------------------------------
$data_root_dir = $lang_data_dir = $split_data_dir = "";
#$data_root_dir = "../../../data/data/"; ## Root dir of data
$data_root_dir = "../../data/data/"; ## Root dir of data
$lang_data_dir = $data_root_dir.$lang."/"; 
$split_data_dir = $lang_data_dir."split_context_features/"; ## Directory of split context features


## Create output dirs and file
##-----------------------------
$out_root_dir = "new_evaluation/";
$out_d1 = $out_root_dir."recomputed_sim_context/";
$out_d2 = $out_d1."PairsStemmed/";
#$out_d3 = $out_d2.$lang."/";

mkdir ($out_d1) unless (-d $out_d1);
mkdir ($out_d2) unless (-d $out_d2);
#mkdir ($out_d3) unless (-d $out_d3);

$out=$infile;
$out=~s/^.+\/([^\/]+)$/$1/;  #remove dir path from filename
$out_sim_file = $out_d2.$out.".sim";


open (O,">$out_sim_file") && printf (STDERR "$0: Writing output PairsStemmed sim file $out_sim_file\n") || die ("$0: cannot write $out_sim_file\n");



## Compute:
## cosine similarity
##------------------
$r = "";
$c = 0;
open (I,$infile) && printf (STDERR "$0: Reading similarity matrix $infile\n")|| die ("$0: Cannot open similarity matrix $infile\n"); ## Process  input file line-by-line
while ($r = <I>)
 {
   $c ++;
   chomp($r);
   #print ($c,". Processing input: ",$r,"\n");

   ## Get the 2 basic fields of input
   ##--------------------------------
   $set1str = $set2str = "";
   @set1 = @set2 = ();
   ($set1str,$set2str) = split(/\s+/,$r);
   @set1 = split(/,/,$set1str);
   @set2 = split(/,/,$set2str);

   ## Iterate over the elements of the two sets
   ##-------------------------------------------
   $re_computed_sim = 0;
   $el1 = $el2 = "";
   %V1 = %V2 = ();
   foreach $el1 (@set1)
    {
      $current_cooc_freq_file = "";
      $current_cooc_freq_file = $split_data_dir.$el1;
      open (CF,$current_cooc_freq_file) || die "$0: cannot open $current_cooc_freq_file\n";
      $cooc_freq_str = <CF>;
      close (CF);
      @entries = ();
      @entries = split(/\s+/,$cooc_freq_str);
      foreach (@entries)
       {
         ($id,$freq) = split(/,/,$_);
         $V1{$id} = 1;
       }
    } ## For each element of set 1

   foreach $el2 (@set2)
    {
      $current_cooc_freq_file = "";
      $current_cooc_freq_file = $split_data_dir.$el2;
      open (CF,$current_cooc_freq_file) || die "$0: cannot open $current_cooc_freq_file\n";
      $cooc_freq_str = <CF>;
      close (CF);
      @entries = ();
      @entries = split(/\s+/,$cooc_freq_str);
      foreach (@entries)
       {
         ($id,$freq) = split(/,/,$_);
         $V2{$id} = 1;
       }
    } ## For each element of set 2   


  $lenV1 = $lenV2 = 0;
  @l1 = @l2 = ();
  @l1 = keys(%V1);
  @l2 = keys(%V2);
  $lenV1 = $#l1 + 1;
  $lenV2 = $#l2 + 1;

  $num_common = 0;
  foreach (keys(%V1))
   {
     if (defined($V2{$_}))
      {
        $num_common++;
      }   
   }


 ## Re-computed similarity
 ## (cosine)
 ##-----------------------
 if (($lenV1>0)&&($lenV2>0))
  {
   $re_computed_sim = $num_common / (sqrt($lenV1*$lenV2));
  }
 else
  {
   $re_computed_sim = 0;
  }

 #print ("Re-computed similarity: ",$re_computed_sim,"\n");
 print O ($re_computed_sim,"\n"); ## Write similarity to output file

 } ## For each line of input file
 
close (I);
close (O);
