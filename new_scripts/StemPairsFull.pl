#!/usr/bin/perl -w

use diagnostics;

# Input: RuleMatrices/language.voc.ruleMatrix1.ids
# Format: ruleID(text) \t stemID(text) \t word_id \t wSet_ids \n

# Resource: evaluation/datasets/language.pairs.ids
# Format: wordA_id \t wordB_id \n

# Output: new_evaluation/datasets/$language\.stempairSets1
# Format: wordA_id Set \t wordB_id Set \n

# use: perl new_scripts/StemPairsFull.pl new_data/RuleMatrices/*.voc.ruleMatrix1.ids

@files=@ARGV;       # input filenames

foreach $file (@files){

	$lang=$outfile=$resource="";

	$lang=$file;
	$lang=~s/^([^\/]+\/)*//;	 # remove path
	$lang=~s/^([^\.]+)\..+$/$1/; # keep lang info only from *.voc.ruleMatrix1.ids 

	$outfile=("new_evaluation/datasets/$lang\.stempairSets1");
	$resource=("evaluation/datasets/$lang\.pairs.ids");

	open (I,"$file") && printf(STDERR "Reading input file $file\n") || die ("cannot read input file: .. $file ..\n");

		%w2wSet=();

		while ($l = <I>){
        	 $l=~s/\s*$//;

        	 $ruleID=$stemID=$word_id=$wSet_ids="";

         	($ruleID, $stemID, $word_id, $wSet_ids)=split("\t", $l); 
         	$w2wSet{$word_id}=$wSet_ids;

    	}#while_file
	close(I);

	open (R,"$resource") && printf(STDERR "Reading resource pairs id file $resource\n") || die ("cannot read resource pairs id file: .. $resource ..\n");
	open (OUT,">$outfile") && printf(STDERR "Writing output pair Sets file $outfile\n") || die ("cannot write output pair Sets file: .. $outfile ..\n");

		$lineNo=0;
		
		while ($l = <R>){
        	 $l=~s/\s*$//;
        	 $lineNo++;

        	 $wA=$wB=$wSetA=$wSetB="";

        	 ($wA, $wB)=split("\t", $l);

        	 if(defined $w2wSet{$wA}){
        	 	$wSetA=$w2wSet{$wA};
        	 }
        	 else{
        	 	$wSetA=$wA;
        	 }
        	 if(defined $w2wSet{$wB}){
        	 	$wSetB=$w2wSet{$wB};
        	 }
        	 else{
        	 	$wSetB=$wB;
        	 }

        	 print OUT ("$wSetA\t$wSetB\n");

		}#while_resource
	close(OUT);
	close(R);


%w2wSet=();
}#foreach_file 