#!/usr/bin/perl -w

use Encode;
use utf8;

# Input: new_data/RuleSubSets_RulFreq/*
# new_data/RuleSubSets_RulFreq/simtype.$language.rulgdf.$subset(lines).$i(part)

$correlations=("new_evaluation/correlations_RulFreq");
$corTables=("new_evaluation/correlation_tables_RulFreq");
$cordata=("new_evaluation/correlation_data_RulFreq");

mkdir ($correlations) unless (-d $correlations);
mkdir ($corTables) unless (-d $corTables);
mkdir ("$cordata") unless (-d "$cordata");

$ext=(".RulFreq.");

foreach $file (@ARGV){
	
	$info=$file;
	$info=~s/.+\/([^\/]+)$/$1/;
	($simtype, $lang, $gdext, $lineNoext, $part)=split(/\./,$info);

	$pairs=("evaluation/datasets/" . $lang . ".pairs.ids");
	$pairSets=("new_evaluation/datasets/" . $lang . ".stempairSets".$ext. $simtype . "." . $part . ".ids");
	$mapfile=("new_data/RuleMatrices/" . $lang . ".voc.ruleMatrix".$ext . $simtype . "." . $part . ".ids");

	$cooc=("new_evaluation/recomputed_sim_cooc/" . $lang . ".stempairSets".$ext . $simtype . "." . $part . ".ids.sim");
	$cordataCooc=($cordata. "/" 	. $lang . "." . $simtype . ".Coocsim." . $part . "_human");

	$conPair=("new_evaluation/recomputed_sim_context/PairsStemmed/"  . $lang . ".stempairSets".$ext . $simtype . "." . $part .".ids.sim");
	$cordataConPair=($cordata. "/"  . $lang . "." . $simtype . ".ConPairsim." . $part . "_human");

	$conCTX=("new_evaluation/recomputed_sim_context/ContextStemmed/" . $lang .".pairs".$ext . $simtype . "." . $part . ".ids.sim");
	$cordataConCTX=($cordata. "/"  	. $lang . "." . $simtype . ".ConCTXsim." . $part . "_human");

	$conB=("new_evaluation/recomputed_sim_context/BothStemmed/" . $lang . ".stempairSets".$ext . $simtype . "." . $part .".ids.sim");
	$cordataConB=($cordata. "/"  	. $lang . "." . $simtype . ".ConBsim." . $part . "_human");


	system(`perl new_scripts/FilterRules_RulFreq.pl $part $lang $simtype $lineNoext new_data/RuleMatrices/$lang\.voc.ruleMatrix1.ids`);

	system(`perl new_scripts/reCooSim.pl $pairSets $lang`);
	system(`perl new_scripts/reConSim_pairsStem.pl $pairSets $lang`);
	system(`perl new_scripts/reConSim_bothStem.pl $pairSets $mapfile $lang`);
	system(`perl new_scripts/reConSim_contextStem.pl $pairs $mapfile $part $simtype $lang $ext`);

	system(`paste evaluation/datasets/$lang.humanScores $cooc > $cordataCooc`);
	system(`perl scripts/PearsonCor.pl -i $cordataCooc > $correlations/$lang.$simtype.Coocsim.CorSim.$part`);

	system(`paste evaluation/datasets/$lang.humanScores $conPair > $cordataConPair`);
	system(`perl scripts/PearsonCor.pl -i $cordataConPair > $correlations/$lang.$simtype.ConPairsim.CorSim.$part`);

	system(`paste evaluation/datasets/$lang.humanScores $conCTX > $cordataConCTX`);
	system(`perl scripts/PearsonCor.pl -i $cordataConCTX > $correlations/$lang.$simtype.ConCTXsim.CorSim.$part`);

	system(`paste evaluation/datasets/$lang.humanScores $conB > $cordataConB`);
	system(`perl scripts/PearsonCor.pl -i $cordataConB > $correlations/$lang.$simtype.ConBsim.CorSim.$part`);

}#foreach rule part

@langs=("en","el","de");

foreach $lang (@langs){

	# Copy extremes:
	# 1/ Copy no stemming (0)
	system(`cp new_evaluation/correlations/$lang.coocd.Coocsim.CorSim.0 $correlations/$lang.net.Coocsim.CorSim.0`);
	system(`cp new_evaluation/correlations/$lang.coocd.Coocsim.CorSim.0 $correlations/$lang.coocd.Coocsim.CorSim.0`);
	system(`cp new_evaluation/correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.coocd.ConPairsim.CorSim.0`);
	system(`cp new_evaluation/correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.net.ConPairsim.CorSim.0`);
 	system(`cp new_evaluation/correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.coocd.ConCTXsim.CorSim.0`);
 	system(`cp new_evaluation/correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.coocd.ConBsim.CorSim.0`);
 	system(`cp new_evaluation/correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.net.ConCTXsim.CorSim.0`);
 	system(`cp new_evaluation/correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.net.ConBsim.CorSim.0`);

 	# 2/ Copy full stemming (10)
 	system(`cp new_evaluation/correlations/$lang.coocd.Coocsim.CorSim.10 $correlations/$lang.coocd.Coocsim.CorSim.10`);
 	system(`cp new_evaluation/correlations/$lang.net.Coocsim.CorSim.10 $correlations/$lang.net.Coocsim.CorSim.10`);

 	system(`cp new_evaluation/correlations/$lang.coocd.ConPairsim.CorSim.10 $correlations/$lang.coocd.ConPairsim.CorSim.10`);
 	system(`cp new_evaluation/correlations/$lang.net.ConPairsim.CorSim.10 $correlations/$lang.net.ConPairsim.CorSim.10`);

	system(`cp new_evaluation/correlations/$lang.coocd.ConCTXsim.CorSim.10 $correlations/$lang.coocd.ConCTXsim.CorSim.10`);
	system(`cp new_evaluation/correlations/$lang.net.ConCTXsim.CorSim.10 $correlations/$lang.net.ConCTXsim.CorSim.10`);

	system(`cp new_evaluation/correlations/$lang.coocd.ConBsim.CorSim.10 $correlations/$lang.coocd.ConBsim.CorSim.10`);
	system(`cp new_evaluation/correlations/$lang.net.ConBsim.CorSim.10 $correlations/$lang.net.ConBsim.CorSim.10`);

}#Copy extremes for each language


system(`perl new_scripts/makeCorTables.pl $corTables $correlations/*`);
