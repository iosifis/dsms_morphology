#!/usr/bin/perl -w

use Encode;
use utf8;

# Input: new_data/RuleSubSets/*

$correlations=("new_evaluation/correlations");
$corTables=("new_evaluation/correlation_tables");
$cordata=("new_evaluation/correlation_data");

mkdir ($correlations) unless (-d $correlations);
mkdir ($corTables) unless (-d $corTables);
mkdir ("$cordata") unless (-d "$cordata");

$ext=("none");

foreach $file (@ARGV){
	
	$info=$file;
	$info=~s/.+\/([^\/]+)$/$1/;
	($simtype, $lang, $gdext, $lineNoext, $part)=split(/\./,$info);

	$pairSets=("new_evaluation/datasets/" . $lang . ".stempairSets." . $simtype . "." . $part . ".ids");
	$mapfile=("new_data/RuleMatrices/" . $lang . ".ruleMatrix." . $simtype . "." . $part . ".ids");
	$pairs=("evaluation/datasets/" . $lang . ".pairs.ids");

	$cooc=("new_evaluation/recomputed_sim_cooc/" . $lang . ".stempairSets." . $simtype . "." . $part . ".ids.sim");
	$cordataCooc=($cordata . "/" 	. $lang . "." . $simtype . ".Coocsim." . $part . "_human");

	$conPair=("new_evaluation/recomputed_sim_context/PairsStemmed/"  . $lang . ".stempairSets." . $simtype . "." . $part .".ids.sim");
	$conCTX=("new_evaluation/recomputed_sim_context/ContextStemmed/" . $lang .".pairs." . $simtype . "." . $part . ".ids.sim");
	$conB=("new_evaluation/recomputed_sim_context/BothStemmed/" 	 . $lang . ".stempairSets." . $simtype . "." . $part .".ids.sim");

	$cordataConPair=($cordata . "/" . $lang . "." . $simtype . ".ConPairsim." . $part . "_human");
	$cordataConCTX=($cordata . "/" 	. $lang . "." . $simtype . ".ConCTXsim." . $part . "_human");
	$cordataConB=($cordata . "/" 	. $lang . "." . $simtype . ".ConBsim." . $part . "_human");

	system(`perl new_scripts/FilterRules.pl $part $lang $simtype $lineNoext new_data/RuleMatrices/$lang\.voc.ruleMatrix1.ids`);
	system(`perl new_scripts/reCooSim.pl $pairSets $lang`);
	system(`perl new_scripts/reConSim_pairsStem.pl $pairSets $lang`);
	system(`perl new_scripts/reConSim_bothStem.pl $pairSets $mapfile $lang`);
	system(`perl new_scripts/reConSim_contextStem.pl $pairs $mapfile $part $simtype $lang $ext`);

	system(`paste evaluation/datasets/$lang.humanScores $cooc > $cordataCooc`);	
	system(`perl scripts/PearsonCor.pl -i $cordataCooc > $correlations/$lang.$simtype.Coocsim.CorSim.$part`);

	system(`paste evaluation/datasets/$lang.humanScores $conPair > $cordataConPair`);
	system(`perl scripts/PearsonCor.pl -i $cordataConPair > $correlations/$lang.$simtype.ConPairsim.CorSim.$part`);
	
	system(`paste evaluation/datasets/$lang.humanScores $conCTX > $cordataConCTX`);
	system(`perl scripts/PearsonCor.pl -i $cordataConCTX > $correlations/$lang.$simtype.ConCTXsim.CorSim.$part`);
	
	system(`paste evaluation/datasets/$lang.humanScores $conB > $cordataConB`);
	system(`perl scripts/PearsonCor.pl -i $cordataConB > $correlations/$lang.$simtype.ConBsim.CorSim.$part`);
	
}#foreach rule part

system(`perl new_scripts/makeCorTables.pl $corTables $correlations/*`);
#system(`perl new_scripts/makePlots.pl $inputCorTables $outplots`);


