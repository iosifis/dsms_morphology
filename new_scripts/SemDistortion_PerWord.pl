#!/usr/bin/perl

# Version Per Word: new semantic distortion estimation based on 
# Word stem

use Encode;
use utf8;
use Statistics::Descriptive;

# Expected Input file format:
#------------------------------
# RuleID<tab>StemID<tab>Wi_ID<tab>WordSet_IDs<tab>SimilaritiesSet<newline>
# where: WordSet_IDs and SimilaritiesSet are comma separated lists

# Input dir: data/SimOfRulesMatrices/sim.$simtype[net|cooc].$language
# Output dir: data/WordDistortion/$simtype.$language.rulword

$outdir=("new_data/GlobalDistortion_PerWord");
mkdir ($outdir) unless (-d $outdir);


@files=@ARGV;       # input filenames

foreach $file (@files){

	$dir=$simtype=$language="";

	$info=$file;
	($dir, $simtype, $language)=split(/\./, $info);

	#-------------------------------------------
	# Get word frequency information as array:
	#-------------------------------------------

	 $freqResource=("data/FrequenciesOfVocabWords/vocab.$language.freq");

	 @frequ=();

	 open (R,"$freqResource") && printf(STDERR "$0: Reading frequencies file $freqResource\n") || die ("$0: cannot read frequencies file: .. $freqResource ..\n");

	 	@frequ=<R>;

	 close(R);

	#-----------------------------------------------
	# Now process similarity file:
	#-----------------------------------------------

	open (I,"$file") && printf(STDERR "$0: Reading input file $file\n") || die ("$0: cannot read input file: .. $file ..\n");

		%ruleSemDistortion=();
		%ruleApplFrequ=();
		
		while ($l = <I>){
        	 $l=~s/\s*$//;
         	 $l= decode('UTF-8', $l);

         	 @all=@data=();
         	 $sims=$wi_ID=$rule_ID="";

         	 $stat=$mean=$distance_sim=$frequ_wi=0;

         	 #-----------------------------------
         	 # Calculate s(Wi, Lk(Wi)) 
         	 #		as average similarity between Wi and set of words with common stem to word Wi:

         	 @all=split("\t", $l);


         	 if(scalar(@all) == 5){ 		# if there is a sim value, element No 4 might be empty...

         	 	 $sims=$all[4];
	         	 @data=split(/\,/, $sims);
	         	 $wi_ID=$all[2];		# element at position 2 should be the Wi ID number
	         	 $rule_ID=$all[0];

	         	 # Do not calculate if rule_ID is NULL:
	         	 
	         	 unless ($rule_ID eq "NULL") {
	         	 	
		         	 $ruleApplFrequ{$rule_ID}++;

		         	 $stat = Statistics::Descriptive::Full->new();	# Create a new statistics object
		         	 $stat->add_data(@data);						# Add data to the statistics variable

		         	 # Values in net similary may be above 1, 
		         	 # for this reason we can normalise as 0-1 
		         	 # by dividing average by number of neighbours in net only:
		         	 
		         	 if($simtype eq "net"){
		         	 	$mean=$stat->mean();						# Get average semantic similarity
		         	 	$mean=($mean/150);							# normalise net $mean
		         	 }
		         	 else{
		         	 	$mean=$stat->mean();						# Get average semantic similarity
		         	 }

		         	 #-----------------------------------
		         	 # Calculate distance Wi as: 1- s(Wi, Lk(Wi))

		         	 $distance_sim=( 1 - $mean );

		         	 #-----------------------------------
		         	 # Get frequency of w_i:
		         	 			
		         	 $frequ_wi=( $frequ[$wi_ID-1] + 1 );	# make sure frequency is never 0
		         	 										# index of frequ array starts from 0, so $wi_ID-1
		         	 #-----------------------------------
		         	 # Save distortion for each rule:

		         	 $ruleSemDistortion{$wi_ID}{'rule ID'}=$rule_ID;
		         	 $ruleSemDistortion{$wi_ID}{'distortion'}=$distance_sim;
		         	 $ruleSemDistortion{$wi_ID}{'frequ'}=$frequ_wi;

		         }#unless rule_ID eq NULL

	         }#if_there is a sim value

         }#while_input 	 
	close (I);

	@frequ=(); # empty frequ that is no longer needed 

	#--------------------------------------------------
	# Sort by distortion value and print output:	
	#--------------------------------------------------

    $out=("$outdir/$simtype.$language.rulword");
    
    open (O,">$out") && printf(STDERR "$0: Writing output to $out\n") || die ("$0: cannot write to output file: .. $out ..\n");

    	foreach $wi_ID (sort { $ruleSemDistortion{$a}{'distortion'} <=> $ruleSemDistortion{$b}{'distortion'} } keys(%ruleSemDistortion) ) {

    		$distortion=$ruleSemDistortion{$wi_ID}{'distortion'};
    		$frequ_wi=$ruleSemDistortion{$wi_ID}{'frequ'};
    		$rule_ID=$ruleSemDistortion{$wi_ID}{'rule ID'};
    		$rule_ID= encode('UTF-8', $rule_ID);

    		$ruleFrequ=$ruleApplFrequ{$rule_ID};

    		print O ("$wi_ID\t$rule_ID\t$distortion\t$ruleFrequ\t$frequ_wi\n");
    		

    	}#foreach_wi_ID
    
    close(O);

    %ruleSemDistortion=();
	%ruleApplFrequ=();

}#foreach_input_file



