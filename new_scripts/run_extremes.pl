#!/usr/bin/perl -w

@langs=("en","el","de");
@sims=("net", "coocd");
$correlations=("new_evaluation/correlations");


# No stemming:
# runs on pairs.ids

foreach $lang (@langs){

	$pairs=("evaluation/datasets/" . $lang . ".pairs.ids");
	$cooc=("new_evaluation/recomputed_sim_cooc/" . $lang . ".pairs.ids.sim");

	$cordataCooc=("new_evaluation/correlation_data/" 	. $lang . ".coocd.Coocsim.0_human");
	$conPair=("new_evaluation/recomputed_sim_context/PairsStemmed/"  . $lang . ".pairs.ids.sim");
	$cordataConPair=("new_evaluation/correlation_data/" . $lang . ".cooc.ConPairsim.0_human");

	system(`perl new_scripts/reCooSim.pl $pairs $lang`);
	system(`paste evaluation/datasets/$lang.humanScores $cooc > $cordataCooc`); 	
	system(`perl scripts/PearsonCor.pl -i $cordataCooc > $correlations/$lang.coocd.Coocsim.CorSim.0`);
    system(`cp new_evaluation/correlations/$lang.coocd.Coocsim.CorSim.0 $correlations/$lang.net.Coocsim.CorSim.0`);

    system(`perl new_scripts/reConSim_pairsStem.pl $pairs $lang`);
    system(`paste evaluation/datasets/$lang.humanScores $conPair > $cordataConPair`);
 	system(`perl scripts/PearsonCor.pl -i $cordataConPair > $correlations/$lang.coocd.ConPairsim.CorSim.0`);
 	system(`cp $correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.net.ConPairsim.CorSim.0`);
 	system(`cp $correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.coocd.ConCTXsim.CorSim.0`);
 	system(`cp $correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.coocd.ConBsim.CorSim.0`);
 	system(`cp $correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.net.ConCTXsim.CorSim.0`);
 	system(`cp $correlations/$lang.coocd.ConPairsim.CorSim.0 $correlations/$lang.net.ConBsim.CorSim.0`);

}#foreach_language

# full stemming:

$part=10;

foreach $lang (@langs){

	system(`cut -f3,4 new_data/RuleMatrices/$lang.voc.ruleMatrix1.ids > new_data/RuleMatrices/$lang.ruleMatrix.$part.ids`);
	
	$pairSets=("new_evaluation/datasets/" . $lang . ".stempairSets1");
	$mapfile=("new_data/RuleMatrices/" . $lang . ".ruleMatrix.$part.ids");
	$pairs=("evaluation/datasets/" . $lang . ".pairs.ids");

	foreach $simtype(@sims){

		$cooc=("new_evaluation/recomputed_sim_cooc/" . $lang . ".stempairSets1.sim");
		$cordataCooc=("new_evaluation/correlation_data/" 	. $lang . "." . $simtype . ".Coocsim." . $part . "_human");
		
		$conPair=("new_evaluation/recomputed_sim_context/PairsStemmed/" . $lang . ".stempairSets1.sim");
		$conCTX=("new_evaluation/recomputed_sim_context/ContextStemmed/" . $lang .".pairs." . $simtype . "." . $part . ".ids.sim");
		$conB=("new_evaluation/recomputed_sim_context/BothStemmed/" . $lang . ".stempairSets1.sim");
		$cordataConPair=("new_evaluation/correlation_data/" . $lang . "." . $simtype . ".ConPairsim." . $part . "_human");
		$cordataConCTX=("new_evaluation/correlation_data/" 	. $lang . "." . $simtype . ".ConCTXsim." . $part . "_human");
		$cordataConB=("new_evaluation/correlation_data/" 	. $lang . "." . $simtype . ".ConBsim." . $part . "_human");

		system(`perl new_scripts/reCooSim.pl $pairSets $lang`);
		system(`perl new_scripts/reConSim_pairsStem.pl $pairSets $lang`);
		system(`perl new_scripts/reConSim_bothStem.pl $pairSets $mapfile $lang`);
		system(`perl new_scripts/reConSim_contextStem.pl $pairs $mapfile $part $simtype $lang`);

		system(`paste evaluation/datasets/$lang.humanScores $cooc > $cordataCooc`);	
		system(`perl scripts/PearsonCor.pl -i $cordataCooc > $correlations/$lang.$simtype.Coocsim.CorSim.$part`);

		system(`paste evaluation/datasets/$lang.humanScores $conPair > $cordataConPair`);
		system(`perl scripts/PearsonCor.pl -i $cordataConPair > $correlations/$lang.$simtype.ConPairsim.CorSim.$part`);
		
		system(`paste evaluation/datasets/$lang.humanScores $conCTX > $cordataConCTX`);
		system(`perl scripts/PearsonCor.pl -i $cordataConCTX > $correlations/$lang.$simtype.ConCTXsim.CorSim.$part`);
		
		system(`paste evaluation/datasets/$lang.humanScores $conB > $cordataConB`);
		system(`perl scripts/PearsonCor.pl -i $cordataConB > $correlations/$lang.$simtype.ConBsim.CorSim.$part`);

	}#foreach_simtype

}#foreach_language

system(`perl new_scripts/makeCorTables.pl $correlations/*`);











