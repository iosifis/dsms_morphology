#!/usr/bin/perl -w

# input directory (local): correlation_tables/
#          		(server): evaluation/correlation_tables/
# output directory (local): Plots_PerRule/filename.pdf
#                (server): evaluation/Plots_PerRule/filename.pdf

# usage perl makePlots.pl $inputDir $outputDir (dir names without last "/")

# $inputDir=("correlation_tables");
# $outputDir=("Plots_RulFreq"); 

## Input args
##-----------
$inputDir=$outputDir="";
$inputDir =  $ARGV[0]; chomp($inputDir); ## Input dir
$outputDir= $ARGV[1]; chomp($outputDir); ## Output dir

%extensions=();
$extensions{"cooc.Coocsim"}=("Coocc & Coocurrence Similarity");
$extensions{"cooc.ConPairsim"}=("Coocc & Context Similarity/Stemming Pairs Only");
$extensions{"cooc.ConCTXsim"}=("Coocc & Context Similarity/Stemming Context Only");
$extensions{"cooc.ConBsim"}=("Coocc & Context Similarity/Stemming Both Pairs and Context");

$extensions{"net.Coocsim"}=("Network & Coocurrence Similarity");
$extensions{"net.ConPairsim"}=("Network & Context Similarity/Stemming Pairs Only");
$extensions{"net.ConCTXsim"}=("Network & Context Similarity/Stemming Context Only");
$extensions{"net.ConBsim"}=("Network & Context Similarity/Stemming Both Pairs and Context");

%langs=();
$langs{"el"}=("Greek");
$langs{"de"}=("German");
$langs{"en"}=("English");

$plotsettings=("set terminal pdf; set xlabel 'Stemming Rules'; set ylabel 'Correlation'; set xtics (0,10,20,30,40,50,60,70,80,90,100); set yrange [0.15:0.75]; ");

foreach $ext (sort keys %extensions){

	$plotcommand="";
	$First=('\$1');

	foreach $lang (sort keys %langs){

		$title=$langs{$lang};
		$plotcommand.=("\'$inputDir/$lang\.$ext\' u ($First*10):2 w lines title '$title',");

	}#foreach_language

	$plotcommand=~s/\,$//;	#remove last comma
	$outfile=("set output '$outputDir/$ext.pdf'; ");
	$title=$extensions{$ext};
	$plotTitle=("set title '$title';");

	system(`gnuplot -e "$plotTitle $plotsettings $outfile plot $plotcommand"`);


}#foreach_extension



#gnuplot -e "set terminal pdf; set output 'out.pdf'; set xlabel 'Stemming Rules'; set ylabel 'Correlation'; set xtics (0,1,2,3,4,5,6,7,8,9,10); plot 'correlation_tables/el.net.ConBsim' u 1:2 w lines title 'EL', 'correlation_tables/en.net.ConBsim' u 1:2 w lines title 'EN', 'correlation_tables/de.net.ConBsim' u 1:2 w lines title 'DE'"
