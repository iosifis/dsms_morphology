#!/usr/bin/perl -w

# 13 June 2014: 
# 1/ include google metric for cooc: coocd=dice coocg=google
# 2/ outfile name is formed automatically based on input (outdir is provided as argument, line 13)
# 3/ root data dir is absolute (cf line 31)

## Input args
##-----------
$lang = $sim_type = $outdir = $idfile = "";

$lang = $ARGV[0];     chomp($lang);         ## Language: [el|en|de]
$sim_type = $ARGV[1]; chomp($sim_type);     ## Similarity type: [coocd|coocg|cont|net]
$outdir = $ARGV[2];   chomp($outdir);       ## Outdir provided without last /, eg new_data/SimOfRuleMatrices
$idfile  = $ARGV[3];  chomp($idfile);       ## Input file, eg. new_data/RuleMatrices/en.voc.ruleMatrix1.ids 

$outfile=("$outdir/sim.$sim_type\.$lang");

$num_in_args = $#ARGV + 1;
if ($num_in_args != 4){
   print ("Wrong number of input arguments.\n");
   print ("4 input arguments are required.\n");
   die ("HOW TO RUN: perl scriptname.pl [el|en|de]  [coocd|coocg|cont|net] outdir filename\n");
}


## Paths to similarities
##----------------------
$data_root_dir = $lang_data_dir = $cooc_data_dir = $cont_data_dir = $net_data_dir = "";
#$data_root_dir = "../../../data/data/"; ## Root dir of data
$data_root_dir = "/media/4gb/data/data/";
$lang_data_dir = $data_root_dir.$lang."/semantic_similarity/"; ## Dir of semantic similarities for given language
$cooc_data_dir = $lang_data_dir."cooccurance_similarity/"; ##  Dir for co-occurrence-based similarities
$cont_data_dir = $lang_data_dir."context_similarity/"; ## Dir for context-based similarities
$net_data_dir = $lang_data_dir."network_similarity/"; ## Dir for netwrok-based similarities

$similarities_dir = "";
if ($sim_type=~/cooc[dg]/) { $similarities_dir = $cooc_data_dir; }
if ($sim_type eq "cont") { $similarities_dir = $cont_data_dir; }
if ($sim_type eq "net") { $similarities_dir = $net_data_dir; }

$split_data_dir = "";
if ($sim_type eq "coocd") { $split_data_dir = $similarities_dir."split_dice/"; } ## Co-occurrence - Dice metric
if ($sim_type eq "coocg") { $split_data_dir = $similarities_dir."split_google/"; } ## Co-occurrence - Dice metric
if ($sim_type eq "net") { $split_data_dir = $similarities_dir."split_netsumsq/"; } ## Network sum of squared similarities

open(OUT,">$outfile") && printf(STDERR "Writing output file $outfile\n") || die ("Cannot write to $outfile\n");

## Get similarities
##-----------------
$ruleID = $targetID = $list_of_wordIDs = "";

open(I,$idfile) && printf(STDERR "Reading input file $idfile\n") || die ("Cannot open $idfile\n");

while ($r = <I>){
   $ruleID = $targetID = $list_of_wordIDs = "";
   chomp($r);
   
   ($ruleID,$stemID,$targetID,$list_of_wordIDs) = split(/\s+/,$r);
   
   @wlst = ();
   @wlst = split(/,/,$list_of_wordIDs);
   

   ## Open the similarities for the target ID
   ##----------------------------------------
   $targetID_sim_file = "";
   $targetID_sim_file = $split_data_dir.$targetID;
   
   if (($targetID == 135436) && ($lang eq "en")){ 
      $targetID_sim_file = $split_data_dir."58602";
    } # missing sim file same as 58602

   open (S,$targetID_sim_file) || die ("Cannot open similarity file for word with ID $targetID");
    $sims_str = "";
    $sims_str = <S>;
   close (S);
   chomp($sims_str);
   
   @id_sim_list = ();
   @id_sim_list = split(/\s+/,$sims_str);
   %id2sim = ();
   $this_id = $this_sim = "";
   
   foreach (@id_sim_list){
      ($this_id,$this_sim) = split(/,/,$_);
      $id2sim{$this_id} = $this_sim;
    }

    $retrieved_sim_str = "";
    foreach $ww (@wlst){
       $rsim = 0;
       if (defined($id2sim{$ww})){
          $rsim = $id2sim{$ww};
        }
       $retrieved_sim_str .= $rsim.",";
     }
    $retrieved_sim_str =~ s/,$//g;

    print OUT ($ruleID,"\t",$stemID,"\t",$targetID,"\t",$list_of_wordIDs,"\t");    
    print OUT ($retrieved_sim_str,"\n");
 }
close (OUT);
close (I);

