#!/usr/bin/perl -w

use Encode;
use utf8;

# Objective: For each stemming rule, find word groups with common stem

# Version 3 - June 14 2014

# Changes in this version:
# 1/ German umlaut & Eszett are corrected: big change in German data esp. after snowball German stemming has been corrected too
# 2/ Found bug in original StemRules version: 
#    duplicate entries in StemRules output, lines do not correspond to *.stemmed1 input
# In this version there is a different indexing in hashes, by word ID, that prevents duplicate entries

# usage: perl src/StemRules_v3.pl data/Vocabulary/*.stemmed1
# applies on content of data/Vocabulary/*.stemmed1

# (German file after correction of snowball bugs is put in this OLD directory as well, 
#  OLD German stemmed file is there too, but renamed so that it is not processed)

# outputs two files for every input file
# Output 1: data/<language>.voc.ruleMatrix1.text :: created for manual check/debugging, keeps actual word forms in output
# Output 2: data/<language>.voc.ruleMatrix1.ids  :: the actual output with word ids, required by Elias program 

@files=@ARGV;       # input filenames
$outdir=("new_data/RuleMatrices/");

foreach $file (@files){
	$outfile=$file;
  
    $outfile=~s/^([^\/]+\/)*//;           # remove any directory path from input filename
    $outfile=~s/\.stemmed1$/\.ruleMatrix1/;
    $outfile=$outdir.$outfile;

    $out1=($outfile . ".text"); # keeps word forms for checking/debugging
    $out2=($outfile . ".ids");  # same but with word ids (to be used as input for sem sim)

    open (I,"$file") && printf(STDERR "Reading input stemmed voc $file\n") || die ("cannot read input stemmed voc file: .. $file ..\n");

    	%wID2word=();
    	%wID2stem=();
    	%ruleID2wID=();
    	%stem2wIDSet=();
    	%stem2wSet=();

    	$linecount=0;	       # linecount creates word ids based on line number

    	while ($l = <I>){
	         $l=~s/\s*$//;
	         $linecount++;
	         $l= decode('UTF-8', $l);

	         ($w, $stem)=split("\t", $l); 
	         
	         # need these $w transformations, as they are also applied in stemming pre-processing: 

	         $w=lc($w);
	         $w=~tr/[άέίήόύώ]/[αειηουω]/; # remove stress 
	         $w=~tr/[ΐϊϋΰ]/[ιιυυ]/;       # remove diacritics
	         $w=~tr/[äëïüö]/[aeiuo]/;     # remove umlaut # v3 (extended to non-umlaut just in case vowels)
	         $w=~tr/[áéíúó]/[aeiuo]/;     # other general: accents (just in case)
	         $w=~tr/[àèìùò]/[aeiuo]/;
	         $w=~tr/[âêîûô]/[aeiuo]/;
	         $w=~s/ß/ss/g;                # substitute ß  # v3 
	         $w=~s/σ\s*$/ς/;              # restore potential sigma telikon spelling error

	         # find the stemming rule, i.e. the suffix that has been removed from the original
         	 # if nothing was removed, then word eq stem and suffix rule is "NULL"
         	 # if the stem has undergone other manipulation than mere stemming, 
             #    keep last two characters of stem as "rule" 

	         $suffix=$w;
	         $suffix=~s/^($stem)//;

	         if($w eq $stem){
	            $suffix=("NULL");
	         }
	         elsif($w eq $suffix){
	            $suffix=substr($stem, -2);
	         }

	         # Populate the hashes:
	         
	         $wID2word{$linecount}=$w;
	         $wID2stem{$linecount}=$stem;
	         push(@{$ruleID2wID{$suffix}}, $linecount);
	         push(@{$stem2wIDSet{$stem}}, $linecount);
	         push(@{$stem2wSet{$stem}}, $w);

	    }#while_file

    close(I);

    # Write output files based on hashes content:

    open (O1,">$out1") && printf(STDERR "Writing output to $out1\n") || die ("cannot write to output file: .. $out1\n");
    open (O2,">$out2") && printf(STDERR "Writing output to $out2\n") || die ("cannot write to output file: .. $out2\n");

    $ruleFreq=0;

    foreach $ruleID ( keys(%ruleID2wID) ) {    # for each "rule" ie suffix

    	#$ruleFreq=@{$ruleID2wID{$ruleID}};		   # number of array elements is rule application freq

    	foreach $wID (@{$ruleID2wID{$ruleID}}){

    		$w=$wID2word{$wID};
    		$stem=$wID2stem{$wID};
    		@wIDSet=@{$stem2wIDSet{$stem}};
    		@wSet=@{$stem2wSet{$stem}};

    		$wIDSetStr=join("\,", @wIDSet);
    		$wSetStr=join("\,", @wSet);

    		$wordOutString=join("\t", $ruleID, $stem, $w, $wSetStr);    # now format output string as tab separated fields
            $idOutString=join("\t", $ruleID, $stem, $wID, $wIDSetStr);

            $wordOutString= encode('UTF-8', $wordOutString);    # ...in this way we only have to do "encode in UTF-8" on the entire output string rather than the individual variables
            $idOutString= encode('UTF-8', $idOutString);

            print O1 ("$wordOutString\n");
            print O2 ("$idOutString\n");

    	}#foreach_wordID 

	}#foreach_ruleID

    close(O1);
    close(O2);
    
    %wID2word=();
    %wID2stem=();
    %ruleID2wID=();
    %stem2wIDSet=();
    %stem2wSet=();

    $linecount=0;	       # linecount creates word ids based on line number
}#foreach_file




