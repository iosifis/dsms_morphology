#!/usr/bin/perl -w

use Encode;
use utf8;

# Usage: perl RulSubsets.pl /data/GlobalDistortion/*
# ie /data/GlobalDistortion/[net|cooc].[lang].rulgd 


# output file has same name as input + Number of rules ext

$outputPath=("new_data/RuleSubSets_RulFreq/");
mkdir ($outputPath) unless (-d $outputPath);					

foreach $rulefile (@ARGV){

	# Get all rules for given language/simtype:

	open (R,"$rulefile") && printf(STDERR "$0: Reading rule file $rulefile\n") || die ("$0: cannot read rule input file: .. $rulefile ..\n");

			@rules=<R>;

	close(R);

	$info=$rulefile;
	$info=~s/.+\/([^\/]+)$/$1/;
	($simtype, $language, $ext)=split(/\./,$info);



	## Get total number of lines in merged corpus
	##-------------------------------------
	$NoRules=(`wc -l $rulefile`);
	$NoRules=~s/^\s*([\d]+)\s.*\s*/$1/; # get line numbers only

	for($i=1; $i<10; $i++){

		$out=$outputPath.$info; 	# output file has same name as input + Number of rules ext
		
		$factor=($i/10);
		$subset=($factor * $NoRules);
		$subset=int($subset)-1;		# $subset should be an integer and -1 coz @rules starts from 0
		@ruleSub=@rules[0..$subset];

		$out=($out."\.".$subset."\.".$i);		# outfile has $subset extension
		open (O,">$out") && printf(STDERR "$0: Writing rule subset $subset file $out\n") || die ("$0: cannot write rule subset $subset file .. $out ..\n");

			print O (@ruleSub);	# array elements contain newlines

		close(O);

	}#for

}#foreach file


